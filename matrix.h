#ifndef MATRIX_H
#define MATRIX_H
#include "figure.h"
#include <QVector>

class Matrix
{
private:
    int** board;
    int height, width;
    void checkRows(QVector<int> &rowsForDelete);
    void checkCols(QVector<int> &colsForDelete);
    int deleteRowOrCols(int n, bool row = true);
    bool checkFreeSpace(int x, int y, Figure *f);
    int deleteFullLines();
public:
    QVector< QVector<int> > getBoard();
    int setFigure(int x, int y, FigureToBoardInterface *f);
    Matrix(int h, int w);
    ~Matrix() {
        for (int i = 0; i < height; ++i) {
            delete board[i];
        }
        delete board;
    };
};

#endif // MATRIX_H
