#ifndef PRINTMYFIGURE_H
#define PRINTMYFIGURE_H
#include <QGraphicsItem>
#include <QPainter>
#include "board.h"
class PrintMyFigure : public QGraphicsItem {
private:
    QVector<QVector< int > > figure;
    int width;
    int height;
    int yshift, xshift, id;
    float scale;
public:
    PrintMyFigure(int id, int xshift, int yshift, QVector<QVector< int> > figure);
    PrintMyFigure(){}
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QPoint getPos();
    int getId();
protected:
    QBrush getBrush(int id);
    void returnBack();
    void mousePressEvent(QGraphicsSceneMouseEvent *e);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *e);
};

#endif // PRINTMYFIGURE_H
