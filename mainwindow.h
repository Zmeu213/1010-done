#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QtSql>
#include "gamescene.h"
#include "scoreform.h"
#include <QMessageBox>
#include <QDateTime>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionNew_Game_2_triggered();

    void on_actionTop_Results_triggered();

private:
    ScoreForm *sForm;
    GameScene *scn;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
