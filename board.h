#ifndef BOARD_H
#define BOARD_H
#include "matrix.h"
#include "factory.h"
class Board
{
private:
    Matrix *board;
    Factory* figuresFactory;
    FigureToBoardInterface **threeFigures;
    int score, figuresInArray;
    void setFigures();
public:
    void newGame();
    bool putFigureOnBoard(int x, int y, int figureId);
    QVector< QVector<int> > getBoard() {
        return board->getBoard();
    }
    FigureToBoardInterface** getFigures() {
        FigureToBoardInterface** result;
        result = new FigureToBoardInterface*[3];
        for (int i = 0; i < 3; ++i) {
            result[i] = threeFigures[i];
        }
        return result;
    }
    QVector<QVector < QVector<int> > > getQFigures() {
        QVector<QVector<QVector<int> > > result;
        for (int i = 0; i < 3; ++i) {
            if (threeFigures[i])
                result.push_back(threeFigures[i]->getQVector());
            else
                result.push_back(QVector<QVector<int> >());
        }
        return result;
    }
    int getScore();
    Board();
};

#endif // BOARD_H
